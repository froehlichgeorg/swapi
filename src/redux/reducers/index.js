import { FETCH_DATA } from "../constants";

const initState = [];

const dataReducer = (state = initState, action) => {
  switch (action.type) {
    case FETCH_DATA:
      return action.payload;
    default:
      return state;
  }
};

export default dataReducer;
