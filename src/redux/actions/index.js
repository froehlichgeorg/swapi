import { FETCH_DATA } from "../constants";

export const fetchData = (endUrl, setIsLoading) => async (dispatch) => {
  const baseUrl = "https://swapi.dev/api";

  setIsLoading(true);
  const response = await fetch(`${baseUrl}/${endUrl}/`)
    .then((res) => res.json())
    .then((data) => {
      if (data.results) {
        setIsLoading(false);
        return data.results;
      } else {
        setIsLoading(true);
      }
    });

  dispatch({
    type: FETCH_DATA,
    payload: response,
  });
};
