import "./List.scss";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { fetchData } from "../../redux/actions";
import { v4 as uuidv4 } from "uuid";
import Loader from "../Loader/Loader";

const List = ({ url, filteredContent }) => {
  const dispatch = useDispatch();
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    dispatch(fetchData(url, setIsLoading));
  }, [url, dispatch]);

  return isLoading ? (
    <Loader />
  ) : filteredContent ? (
    filteredContent[0] ? (
      <>
        <div className="list">
          <div className="table-title">
            <h2>{url}</h2>
          </div>
          <table className="resource-table">
            <thead>
              <tr>
                {Object.keys(filteredContent[0]).map((value) =>
                  value !== "rotation_period" &&
                  value !== "orbital_period" &&
                  value !== "created" &&
                  value !== "edited" &&
                  value !== "films" &&
                  value !== "residents" &&
                  value !== "species" &&
                  value !== "pilots" &&
                  value !== "starships" &&
                  value !== "vehicles" &&
                  value !== "url" ? (
                    <th key={uuidv4()} className="table-header-item">
                      {value.replace(/_/g, " ")}
                    </th>
                  ) : null
                )}
              </tr>
            </thead>

            <tbody>
              {filteredContent.map((value) => (
                <tr key={uuidv4()}>
                  {Object.keys(filteredContent[0]).map((item) =>
                    item !== "rotation_period" &&
                    item !== "orbital_period" &&
                    item !== "created" &&
                    item !== "edited" &&
                    item !== "residents" &&
                    item !== "films" &&
                    item !== "species" &&
                    item !== "pilots" &&
                    item !== "starships" &&
                    item !== "vehicles" &&
                    item !== "url" ? (
                      <td key={uuidv4()} data-label={item.replace(/_/g, " ")}>
                        <p>{value[item]}</p>
                      </td>
                    ) : null
                  )}
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </>
    ) : null
  ) : null;
};

export default List;
