import loader from "./Loader.module.scss";
import React from "react";

const Loader = () => {
  return (
    <div className={loader["loader-wrapper"]}>
      <div className={loader.loader} />
      <div className={loader["loader-text"]}>Loading...</div>
    </div>
  );
};

export default Loader;
