import "./Search.scss";
import React, { useEffect, useRef, useState } from "react";
import { useSelector } from "react-redux";

const Search = ({ setFilteredContent }) => {
  const inputRef = useRef();
  const data = useSelector((state) => state);
  const [searchValue, setSearchValue] = useState("");

  const searchHandler = (even) => {
    const value = even.target.value;
    setSearchValue(value);
  };

  const clearSearch = () => {
    setSearchValue("");
  };

  useEffect(() => {
    setFilteredContent(
      data.filter(
        (item) =>
          item.name.toLowerCase().includes(searchValue) ||
          (item.climate && item.climate.toLowerCase().includes(searchValue)) ||
          (item.hair_color &&
            item.hair_color.toLowerCase().includes(searchValue)) ||
          (item.skin_color &&
            item.skin_color.toLowerCase().includes(searchValue)) ||
          (item.eye_color &&
            item.eye_color.toLowerCase().includes(searchValue)) ||
          (item.manufacturer &&
            item.manufacturer.toLowerCase().includes(searchValue)) ||
          (item.starship_class &&
            item.starship_class.toLowerCase().includes(searchValue)) ||
          (item.vehicle_class &&
            item.vehicle_class.toLowerCase().includes(searchValue)) ||
          (item.climate && item.climate.toLowerCase().includes(searchValue)) ||
          (item.gravity && item.gravity.toLowerCase().includes(searchValue)) ||
          (item.hair_color &&
            item.hair_color.toLowerCase().includes(searchValue)) ||
          (item.skin_color &&
            item.skin_color.toLowerCase().includes(searchValue)) ||
          (item.eye_color &&
            item.eye_color.toLowerCase().includes(searchValue)) ||
          (item.gender && item.gender.toLowerCase().includes(searchValue)) ||
          (item.terrain && item.terrain.toLowerCase().includes(searchValue))
      )
    );
  }, [searchValue, data, setFilteredContent]);

  useEffect(() => {
    setSearchValue("");
  }, [data]);

  return (
    <div className="search-field">
      <input
        type="text"
        onChange={searchHandler}
        value={searchValue}
        placeholder={"Type you search"}
        ref={inputRef}
      />
      <div className="clear-search" onClick={clearSearch}></div>
    </div>
  );
};

export default Search;
