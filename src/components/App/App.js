import "./App.css";
import List from "../List/List";
import Header from "../Header/Header";
import { useState } from "react";

function App() {
  const [dataType, setDataType] = useState("planets");
  const [filteredContent, setFilteredContent] = useState("");

  return (
    <div className="App">
      <Header
        setDataType={setDataType}
        dataType={dataType}
        setFilteredContent={setFilteredContent}
      />
      <List
        url={dataType}
        filteredContent={filteredContent}
        setFilteredContent={setFilteredContent}
      />
    </div>
  );
}

export default App;
