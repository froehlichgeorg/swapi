import "./Header.scss";
import React from "react";
import HeaderItem from "../HeaderItem/HeaderItem";
import { v4 as uuidv4 } from "uuid";
import Search from "../Search/Search";

const Header = ({ setDataType, setFilteredContent, dataType }) => {
  const headerItems = ["Planets", "People", "Starships", "Vehicles"];

  const dataHandler = (event) => {
    setDataType(event.target.dataset.value.toLowerCase());
  };

  return (
    <div className="header">
      <div className="search">
        <Search setFilteredContent={setFilteredContent} />
      </div>
      <div className="navigation">
        <div className="navigation-items">
          {headerItems.map((item) => (
            <HeaderItem
              buttonName={`${item}`}
              dataHandler={dataHandler}
              setDataType={setDataType}
              isActive={item.toLowerCase() === dataType}
              key={uuidv4()}
            />
          ))}
        </div>
      </div>
    </div>
  );
};

export default Header;
