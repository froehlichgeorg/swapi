import button from "./HeaderItem.module.scss";
import React from "react";

const HeaderItem = ({ buttonName, dataHandler, isActive }) => {
  const classname = isActive ? button.active : button.inactive;

  return (
    <button onClick={dataHandler} data-value={buttonName} className={classname}>
      {buttonName}
    </button>
  );
};

export default HeaderItem;
