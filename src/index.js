import "./index.css";
import React from "react";
import ReactDOM from "react-dom";
import App from "./components/App/App";
import { applyMiddleware, createStore } from "redux";
import dataReducer from "./redux/reducers";
import { Provider } from "react-redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";

const store = createStore(
  dataReducer,
  composeWithDevTools(applyMiddleware(thunk))
);

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("root")
);
